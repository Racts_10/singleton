/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import Singleton.Lab9p2.VegetableFactory;
import java.lang.ProcessBuilder.Redirect.Type;

/**
 *
 * @author Rachit
 */
public class Lab9p1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       VegetableFactory vegetableFactory = VegetableFactory.getInstance();
       Beet beet = vegetableFactory.getBeet(Type.beet);
       System.out.println(beet.describeBeet());
     
       Beet beet2 = vegetableFactory.getBeet(Type.CARROT);
       System.out.println(beet2.describeBeet());
      


    }
    
    
}
